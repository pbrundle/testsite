## PDA Base Theme

#### A starter Wordpress installation with a base theme to kickstart projects that use Wordpress primarily as a CMS with ACF.

### Structure

Theme is stripped of sidebar, comments and other seldom used templates. The theme folder includes a pre-configured Gulp package.

Included WP plugins:

- Advanced Custom Fields PRO
- Duplicate Posts
- Simple Page Ordering
- Regenerate Thumbnails
- WP-PageNavi
- WP Mobile Detect
- wpMandrill
- W3TC
- WooCommerce (on own branch)


### Gulp

Run 'npm install' inside theme folder to download required Node Modules.

Configure Gulpfile.js then run 'gulp' in terminal.


### Database

Install latest mysql database from _db folder.


### Uploads Folder

You will need to create /wp-content/uploads


### ACF

There a couple of starter ACF fields that can be imported from the _acf folder.


### JS

JS should be enqueued via *functions.php*. Plugins are combined into the one minified file via Gulp. Use *js/init.js* for any plugins that need to be loaded in the head.

Included JS plugins:

- Modernizr (feature detection)
- CSS Browser Selector (echo browser name as body class)
- Bootstrap (for commonly used functions and CSS mixins)
- JAIL (lazy load images)
- RoyalSlider or Slick Slider

See Gulpfile.js for including js plugins

Custom JS should be included in *js/scripts.js*. Page specific scripts can be executed using the method detailed here: http://bit.ly/10UzTMI


### SCSS

CSS should be nested, resembling the markup structure, within a limit of 3 nests.

Use media queries inline with mixins, rather than in separate sections.

Font files should be kept in the *css/fonts* directory and loaded via *font-face.css*.

Bourbon Neat is the included grid system, see http://neat.bourbon.io

### Other considerations

Here are a few things we can do to make using WP a pain-free experience for our clients.

- Use targeted Advanced Custom Fields instead of default WYSIWYG content editor
- Avoid using shortcodes if possible
- Avoid attaching images inside of WYSIWYG or content fields if possible (use ACF image field)