<?php

// required for W3 Total Cache mfunc
// define('W3TC_DYNAMIC_SECURITY', 'qwertyuiop');


/*-----------------------------------*/
/*  Check environment
/*-----------------------------------*/

if ($_SERVER["HTTP_HOST"] === 'www.wordpress.dev') {

	define('LOCALHOST', true);

	$host = 'localhost';
	define('WP_HOME','http://www.wordpress.dev');
	define('WP_SITEURL','http://www.wordpress.dev');
	$db_name = 'wordpress';
	$db_user = 'root';
	$password = 'root';
	

} else {

	define('LOCALHOST', false);
	// define('WP_HOME','http://www.studiopda.com.au');
	// define('WP_SITEURL','http://www.studiopda.com.au');

	$host = 'localhost';
	$db_name = '';
	$db_user = '';
	$password = '';
}

define('DB_NAME', $db_name);
define('DB_USER', $db_user);
define('DB_PASSWORD', $password);
define('DB_HOST', $host);



/*-----------------------------------*/
/*  Turn on debug, turn up memory
/*-----------------------------------*/

define('WP_DEBUG', true);
define( 'WP_MEMORY_LIMIT', '64M' );



/*-----------------------------------*/
/*  Default WP config
/*-----------------------------------*/

define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('AUTH_KEY',         'gjg|h?}]ic9XGb>X[;ucl|2j*(f&0?1AQy;o6|-TRV04+v],J-cmG&]Z+7EkHr+M');
define('SECURE_AUTH_KEY',  'p5N/Y0dl69[h<f:eG 11IG{R`0.}OP5Neq%8j/D,R 6h9|p[1t2TKg5#G?m&nr_d');
define('LOGGED_IN_KEY',    'o4:op?*6QUorP+[QoqPvRu[X?X>10M~e2q3N%?%P)->C+PExzI7Ssx=f_s&xKJy4');
define('NONCE_KEY',        '/v B=x}-i8c]sN9{*Tw-yf<SvpP#8gp-|0P/`(N|P8);v#@VcLF@Ld?[+0/MUab&');
define('AUTH_SALT',        'PzKE9ygUZh:Z-wW-aE%!5<g$Ke|P.|c6SfuY>yfv/b3ewY4 `1(qe7MqO@+ceM3]');
define('SECURE_AUTH_SALT', ']>LT782vehU|!x1rY2l=U,DwP;87huzuESz;-|pW`xhQP}}-RK(:KM*+^K3e].9U');
define('LOGGED_IN_SALT',   '5d296W&m2d4E)1#sqPFw;NZf/el:C-bo*g7#lKZMw~Qa]71NE+a,U6(^FRq[hrf$');
define('NONCE_SALT',       '!lYo+4fWp}^X);,#di!YoM98xnVC-.??n00puPZK|=VFl#W,ih-g}U4l%Xc>LO19');



$table_prefix  = 'wp_';
define('WPLANG', '');
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Composer Autoloader */
require_once(ABSPATH.'vendor/autoload.php');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
