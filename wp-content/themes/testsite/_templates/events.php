<?php
/*
Template Name: Events
*/

	/*-----------------------------------*/
	/*  Events Listing Template
	/*-----------------------------------*/

?>

<?php get_header(); ?>


<div class="content">

	<?
	/*------------------------------------------------------------------------*/
	/*	Slider
	/*------------------------------------------------------------------------*/
	?>

	<div class="full-width slider-wrapper">

		<div class="slider-container">

			<div class="slider">

				<?
				$page_id = $post->ID;
				include(locate_template('_inc/inc.slider-items.php'));
				?>

			</div>
		</div>
	</div>


	<?
	/*------------------------------------------------------------------------*/
	/*	Breadcrumbs
	/*------------------------------------------------------------------------*/
	?>



	<div class="breadcrumbs">
		<div class="container">
			<a href="<?= site_url(); ?>">Home</a> > What's On
		</div>
	</div>


	<section id="events">

		<div class="container">


			<?
			/*------------------------------------------------------------------------*/
			/*	Query String / Filters
			/*------------------------------------------------------------------------*/

			$types_string_arr = array();
			if(isset($_GET['type'])){
				$types_string_arr = explode(',', $_GET['type']);
			}
			?>




			<?
			/*------------------------------------------------------------------------*/
			/*	Title
			/*------------------------------------------------------------------------*/
			?>

			<h1>Current &amp; Upcoming Events</h1>



			<?
			/*------------------------------------------------------------------------*/
			/*	Filtering
			/*------------------------------------------------------------------------*/
			?>

			<div id="filtering">

				<h2>Filter by:</h2>
				<a class="about" id="closer" href="#">Filter by: <span><i class="fa fa-chevron-down"></i></span></a>
				<?
				$event_types = get_terms('event-types'); ?>

				<ul class="list">
					<li>
						<label>
							<input type="checkbox" name="filter_all" id="type-all" <? if(empty($types_string_arr)){?>checked<? } ?> />
							<span></span> All
						</label>
					</li>
					<? foreach($event_types as $type){
						if($type->slug!='community'){?>
							<li>
								<label for="type-<?= $type->slug; ?>">
									<input type="checkbox" name="type_filter" class="checkbox" id="type-<?= $type->slug; ?>"
									<? if(in_array($type->slug, $types_string_arr)){ ?>checked<? } ?> />
									<span></span><?= $type->name; ?>
								</label>
							</li>
						<? } ?>
					<? } ?>
				</ul>

				<br class="hurdle"/>
			</div>
			<br class="hurdle"/>



			<?
			/*------------------------------------------------------------------------*/
			/*	List
			/*------------------------------------------------------------------------*/
			?>

			<div class="outer-container">

				<? 
				// https://wordpress.org/support/topic/order-posts-on-meta_query-with-relation
				add_filter( 'get_meta_sql', 'get_meta_sql_datespans' );

				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'events',
					'posts_per_page' => 12,
					'paged' => $paged,
					'meta_key' => 'date_from',
					'orderby' => 'meta_value', //meta_value_num
					'order' => 'ASC',

					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key' => 'date_from',
							'value' => date('Ymd'),
							'type' => 'numeric',
							'compare' => '>='
						),
						array(
							'key' => 'date_to',
							'value' => date('Ymd'),
							'type' => 'numeric',
							'compare' => '>='
						)
					)			
				);

				// Taxonomy Filtering

				if(!empty($types_string_arr)){

					//filtered posts
					$args['tax_query'] = array(
				        array(
				            'taxonomy'  => 'event-types',
				            'field'     => 'slug',
				            'terms'     => $types_string_arr,
						)
					);
				} else {

					// all posts
					$args['tax_query'] = array(
				        array(
				            'taxonomy'  => 'event-types',
				            'field'     => 'slug',
				            'terms'     => 'community', //exclude community posts
				            'operator'  => 'NOT IN'
						)
					);
				}



				$posts = new WP_Query( $args );
				remove_filter( 'get_meta_sql', 'get_meta_sql_datespans' );

				while ( $posts->have_posts() ) : $posts->the_post();

					include(locate_template('_inc/inc.event-item.php'));

				endwhile;
				?>
			</div>


			<?
		    /*------------------------------------------------------------------------*/
			/*	Pagination
			/*------------------------------------------------------------------------*/
			?>
			<br class="hurdle"/>
			<div class="pagination">
				<? wp_pagenavi( array( 'query' => $posts )); ?>
			</div>

			<? wp_reset_query(); ?>

		</div>

	</section>

</div>

<?php get_footer(); ?>
