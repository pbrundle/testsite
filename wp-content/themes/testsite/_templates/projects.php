<?php
/*
Template Name: Projects
*/

	/*-----------------------------------*/
	/*  Project Listing Template
	/*-----------------------------------*/

?>

<?php get_header(); ?>


<section id="projects" class="content">

	<div class="container">
		

		<?
		// get list
		$args = array(
			'post_type' => 'projects',
			'posts_per_page' => -1,
			'orderby'	 => 'menu_order', // using Simple Page Ordering plugin
			'order'	 => 'asc',
		);
		$my_query = new WP_Query($args);
		while ($my_query->have_posts()) : $my_query->the_post(); 
		?>

			<div class="item">

				<div class="info">

					<h2><a href="<?= the_permalink(); ?>"><?= get_the_title(); ?></a></h2>
					<p><?= get_field('description'); ?></p>

				</div>

				<p class="credit"><?= get_field('list_image_credit'); ?></p>

				<a class="cover"  href="<?= the_permalink(); ?>" style="background:rgba(0,0,0,<?= get_field('image_opacity'); ?>);"></a>
				<? 
		        $attachment_id = get_field('list_image');
		        $img = wp_get_attachment_image_src( $attachment_id, 'm');
		        ?>

		        <div class="thumb" style="background-image: url('<? echo $img[0]; ?>');"></div>



			</div>


		<? endwhile; ?>
	
		<?php wp_reset_query(); ?>
	
	</div>

</section>

<?php get_footer(); ?>