<?
	/*-----------------------------------*/
	/*  Loop blog posts
	/*-----------------------------------*/
?>

<? while ( have_posts() ) : the_post() ?>

	<article class="clearfix">

		<div class="meta">
			<h2><a href="<? the_permalink() ?>"><? the_title(); ?></a></h2>
			<h3><? the_time('d F Y') ?> <span>in <a href="#"><?php the_category(' & ') ?></a></span></h3>
		</div>

		<div class="post">
		  <?php the_content(); ?>
		</div>

	</article>

<? endwhile; ?>