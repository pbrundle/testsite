<?php

	/*-----------------------------------*/
	/*  Single Project Template
	/*-----------------------------------*/

?>

<?php get_header(); ?>



<section class="<? echo $post->post_name;?>">

	<div class="container">


		<div class="next_project">
			<? 
			//echo previous_post_link('%link','previous project');
			echo next_post_link('%link','Next Project');
			?>
		</div>


		<h2><? the_title(); ?></h2>


		<?  // MAIN CONTENT 
		include('includes/inc.content-loop-project.php');
		?>


	</div>
</section>


<?php get_footer(); ?>