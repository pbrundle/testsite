<?
if( have_rows('feature_slider',$page_id) ):

    while ( have_rows('feature_slider',$page_id) ) : the_row();

		$image = wp_get_attachment_image_src( get_sub_field('image'), 'slider_image' );

		if($image): ?>
			<div>

				<img src="<?= $image[0]; ?>" />

				<? //event (relationship)
				$event_data = get_sub_field('event');
				if(!empty($event_data)):
				?>

					<div class="slick-details">
						<? // category
						$types = wp_get_post_terms( $event_data[0]->ID, 'event-types' );
						if(!empty($types)){ ?>
						<h3><?= $types[0]->name; ?></h3>
						<? } else { ?>
						<h3>Event</h3>
						<? } ?>

						<h2><a href="<?= site_url().'/events/'.$event_data[0]->post_name; ?>"><?= get_sub_field('title');?></a></h2>

						<div class="details">
							<?
							$venues = get_field('venue',$event_data[0]->ID);
							?>
							<div class="venue"><a href="<?= site_url().'/events/'.$event_data[0]->post_name; ?>"><? if(!empty($venues[0])) echo $venues[0]->post_title; ?></a></div>
							<div class="date">
								<a href="<?= site_url().'/events/'.$event_data[0]->post_name; ?>"><?
								if(get_field('date_type',$post->ID)=='span'){

									// see functions.php
									echo event_date_format( get_field('date_from',$event_data[0]->ID), get_field('date_to',$event_data[0]->ID) );

								} else {
									$date_from = DateTime::createFromFormat('Ymd', get_field('date_from',$event_data[0]->ID));
									echo $date_from->format('d F Y');
								}
								?>
<i class="fa fa-chevron-right"></i></a>
							</div>

						</div>

					</div>

				<? endif; ?>

			</div>



    	<?
    	endif;
    endwhile;
endif;
?>