<?
$use_google_map = false;


if( have_rows($content_var) ):

    while ( have_rows($content_var) ) : the_row();


 		/*------------------------------------------------------------------------*/
		/*	Heading
		/*------------------------------------------------------------------------*/
        if( get_row_layout() == 'heading' ): ?>

        	<h2><?= get_sub_field('content'); ?></h2>


        <?
        /*------------------------------------------------------------------------*/
		/*	Intro text
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'intro_text' ): ?>

        	<div class="intro"><?= get_sub_field('content'); ?></div>


        <?
        /*------------------------------------------------------------------------*/
		/*	Text
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'text' ): ?>

        	<div class="text"><?= get_sub_field('content'); ?></div>


        <?
        /*------------------------------------------------------------------------*/
		/*	Image
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'image' ): ?>


        	<div class="image_container">
				<? $image = wp_get_attachment_image_src(get_sub_field('image'), 'large'); ?>
				<img src="<?= $image[0]; ?>">
				<? $caption = get_sub_field('caption');
				if(!empty($caption)){ ?>
				<div class="caption"><?= $caption; ?></div>
				<? } ?>
			</div>


		<?
		/*------------------------------------------------------------------------*/
		/*	Logo
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'logo' ): ?>

        	<div class="logo_container">
				<? $image = wp_get_attachment_image_src(get_sub_field('image'), 'full'); ?>
				<img src="<?= $image[0]; ?>">
			</div>


		<?
		/*------------------------------------------------------------------------*/
		/*	Video
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'video' ):

        	$video = get_sub_field('content');
			?>
			<div class="video_container"><?= $video; ?></div>



		<?
		/*------------------------------------------------------------------------*/
		/*	External link
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'external_link' ):

			$link = get_sub_field('url');
			$linking_text = get_sub_field('link_text');
			if(empty($linking_text)) $linking_text = 'Link';

			if(!empty($link)){ ?>
				<p class="external_link"><a href="<?= $link; ?>"><?= $linking_text; ?></a></p>
			<? } ?>



		<?
		/*------------------------------------------------------------------------*/
		/*	Internal link
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'internal_link' ):

			$link = get_sub_field('link');
			$linking_text = get_sub_field('linking_text');
			if(empty($linking_text)) $linking_text = 'Link';

			if(!empty($link)){ ?>
				<p class="internal_link"><a href="<?= $link; ?>"><?= $linking_text; ?></a></p>
			<? } ?>




		<?
		/*------------------------------------------------------------------------*/
		/*	Promo Image / Link
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'promo' ):

			$link_type = get_sub_field('link_type');
			$image = wp_get_attachment_image_src(get_sub_field('image'), 'large');

			switch ($link_type) {
				case 'none':
					?>
					<div class="promo"><img src="<?= $image[0]; ?>" /></div>
					<?
					break;
				case 'external':
					?>
					<div class="promo"><a href="<? the_sub_field('external_link'); ?>"><img src="<?= $image[0]; ?>" /></a></div>
					<?
					break;
				case 'internal':
					?>
					<div class="promo"><a href="<? the_sub_field('internal_link'); ?>"><img src="<?= $image[0]; ?>" /></a></div>
					<?
					break;
			}
			?>


		<?
        /*------------------------------------------------------------------------*/
		/*	EventBrite
		/*------------------------------------------------------------------------*/
        elseif( get_row_layout() == 'eventbrite' && $content_var != 'sidebar_content'): ?>

        	<div class="eventbrite"><?= get_sub_field('content'); ?></div>






        <?
		/*------------------------------------------------------------------------*/
		/*	File Downloads
		/*------------------------------------------------------------------------*/
		elseif( get_row_layout() == 'file_downloads' ): 

			?>

			<div class="file_downloads">

				<h2><?= get_sub_field('heading'); ?></h2>

				<ul>
				<?
				while( have_rows('files') ): the_row();

					$file_id = get_sub_field('file');
					$file_path = wp_get_attachment_url( $file_id );
					$linking_text = get_sub_field('title');
					$description = get_sub_field('description');
					if(empty($linking_text)) $linking_text = 'Download File';

					if(!empty($file_path)){ ?>
						<li class="file">
							<a class="file_icon" href="<?= $file_path; ?>"><img src="<?php bloginfo('template_directory'); ?>/images/download.jpg"/></a>
							<a href="<?= $file_path; ?>" target="_blank"><?= $linking_text; ?></a>
							<?= $description; ?>
						</li>
					<? } ?>

				<?
				endwhile; ?>
				</ul>
			</div>




		<?
		/*------------------------------------------------------------------------*/
		/*	Google Map
		/*------------------------------------------------------------------------*/
		elseif( get_row_layout() == 'map' ): 

			$use_google_map = true;
			$map_data = get_sub_field('content')
			?>

			<div class="location-map">
				<div class="marker" data-lat="<? echo $map_data['lat']; ?>" data-lng="<? echo $map_data['lng']; ?>"></div>
			</div>

        <?
        endif;

    endwhile;

endif;



if($use_google_map){
?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script type="text/javascript">
(function($) {



	/*-----------------------------------*/
	/*  Map Functions
	/*-----------------------------------*/

	function render_map( $el ) {

		// var
		var $markers = $el.find('.marker');

		// vars
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};

		// create map
		var map = new google.maps.Map( $el[0], args);

		// add a markers reference
		map.markers = [];

		// add markers
		$markers.each(function(){
			add_marker( $(this), map );
		});

		// center map
		center_map( map );

	}

	function add_marker( $marker, map ) {

		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {

				infowindow.open( map, marker );

			});
		}

	}

	function center_map( map ) {

		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){

			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});

		// only 1 marker?
		if( map.markers.length == 1 ){
			// set center of map
			map.setCenter( bounds.getCenter() );
			map.setZoom( 16 );
		} else {
			// fit to bounds
			map.fitBounds( bounds );
		}

	}




	$(document).ready(function(){

		$('.location-map').each(function(){

			render_map( $(this) );

		});


	});

})(jQuery);
</script>



<? } ?>

