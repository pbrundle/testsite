
project_type = :stand_alone
http_path = "/"
sass_dir = "stylesheets"
css_dir = "./"
images_dir = "images"
fonts_dir = "fonts"
javascripts_dir = "js"
line_comments = false
preferred_syntax = :scss
output_style = :expanded
relative_assets = true