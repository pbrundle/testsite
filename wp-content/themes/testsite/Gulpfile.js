

/*-----------------------------------*/
/*  Config
/*-----------------------------------*/

var browserSyncProxy = "www.wordpress.dev";


// specify JS plugins to load in head
var js_plugins_init = [
	'js/plugins/modernizr.custom.js',
	'js/plugins/pace.min.js'
];


// specify JS plugins to load in footer
var js_plugins = [
	
	//'js/plugins/bootstrap.js',
	//'js/plugins/fancybox/jquery.fancybox.js',
    //'js/plugins/royalslider/dev/jquery.royalslider.js',
    //'js/plugins/royalslider/dev/modules/jquery.rs.global-caption.js',
    //'js/plugins/royalslider/dev/modules/jquery.rs.auto-height.js',
    'js/plugins/slick/slick/slick.js'
];

// sass: shouldn't need to change this
var stylesPath = "stylesheets/style.scss";



/*-----------------------------------*/
/*  Require
/*-----------------------------------*/


var gulp = require('gulp'),
    compass = require('gulp-compass'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
	pngcrush = require('imagemin-pngcrush'),
    svgSprite = require('gulp-svg-sprites');   






/*-----------------------------------*/
/*  Sass
/*-----------------------------------*/

gulp.task('styles', function() {

	return gulp.src(stylesPath)
    .pipe(plumber({errorHandler: notify.onError("Error!")}))
	.pipe(compass({
		config_file: 'config.rb',
		css: './',
		sass: 'stylesheets'
	}))
	.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1','ios 6','android 4'))
	.pipe(gulp.dest('./'))
	.pipe(rename({suffix: '.min'}))
	.pipe(minifycss())
	.pipe(gulp.dest('css'))
    //.pipe(browserSync.reload({stream:true, notify: false}))
	.pipe(notify({ message: 'css done' }));;
});






//*-----------------------------------*/
/*  JS
/*-----------------------------------*/

gulp.task('scripts', function() {
  return gulp.src('js/scripts.js')
    //.pipe(jshint('.jshintrc'))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    //.pipe(gulp.dest('js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('js/min'))
    .pipe(browserSync.reload({stream:true, notify: false}))
    .pipe(notify({ message: 'js scripts done' }));
});


gulp.task('js_plugins_init', function() {
  return gulp.src(js_plugins_init)
   	//.pipe(jshint())
    //.pipe(jshint.reporter('default'))
    .pipe(concat('init.js'))
    .pipe(gulp.dest('js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('js/min'))
    .pipe(notify({ message: 'js init plugins done' }));
});

gulp.task('js_plugins', function() {
  return gulp.src(js_plugins)
    .pipe(concat('plugins.js'))
    .pipe(gulp.dest('js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('js/min'))
    .pipe(notify({ message: 'js plugins done' }));
});




/*-----------------------------------*/
/*  svg png sprites
/*-----------------------------------*/

var sprites_folder 		= 'img/icons/*.svg',
	sprites_selector	= "sprite-%f",
	sprites_css_output 	= "../../stylesheets/base/_sprite.scss",
	sprites_svg_output 	= "img/iconsprites/%f",
	sprites_png_output 	= "img/iconsprites/%f",
	sprites_destination	= "img/iconsprites";


gulp.task('sprites', function () {
    return gulp.src(sprites_folder)
	    .pipe(svgSprite({
	    	selector: "sprite-%f",
	    	// preview: false
	    	// mode: "symbols"
	    	// mode: "symbols"
	    	cssFile: sprites_css_output,
	    	svgPath: sprites_svg_output,
	    	pngPath: sprites_png_output,
	    	padding: 0,
	    	common: "sprite"
	    }))
	    .pipe(gulp.dest(sprites_destination))
	    .pipe(filter("**/*.svg"))
	    .pipe(svg2png())
	    .pipe(gulp.dest(sprites_destination));
});



/*-----------------------------------*/
/*  imagemin
/*-----------------------------------*/

gulp.task('minifyimages', function () {
    return gulp.src('img/src/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('img'));
});



/*-----------------------------------*/
/*  Browser Sync
/*-----------------------------------*/

/*gulp.task('browser-sync', function() {
    
    browserSync({
        proxy: browserSyncProxy 
    });
});
*/


/*-----------------------------------*/
/*  Watch
/*-----------------------------------*/

gulp.task('watch', function() {

	// scss
	gulp.watch('stylesheets/**/*.scss', ['styles']);

	// js
	gulp.watch('js/scripts.js', ['scripts']);

	// php 
	//gulp.watch('**/*.php', function() {
    	//browserSync.reload();
 	 //});

});



/*-----------------------------------*/
/*  Deafult Task
/*-----------------------------------*/

gulp.task('default', function() {
    gulp.start('watch');
});


/*-----------------------------------*/
/*  Run All Task
/*-----------------------------------*/

gulp.task('all', function() {

	gulp.start(['scripts','js_plugins_init','js_plugins','styles']);

});


