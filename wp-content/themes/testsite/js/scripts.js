/*----------------------------------------------------------*/
/*
/*	Project  :  Project Title
/*	Author   :  PDA
/*
/*----------------------------------------------------------*/



/*-----------------------------------*/
/*  Page specific JS
/*-----------------------------------*/


site = {

	////
	// Home
	////

	home : {
		init : function(){
			(function($) {

				//console.log('HOME PAGE!');

			})( jQuery );
		}
	},

	about : {
		init : function(){
			(function($) {

				console.log('ABOUT PAGE!');

			})( jQuery );
		}
	}

};


/*-----------------------------------*/
/*  Global JS
/*-----------------------------------*/

(function($) {

	////
	//	Pace finished loading
	////

		Pace.on('done', function() {
			// console.log('ASSETS ARE LOADED!');
		});

})( jQuery );



/*-----------------------------------*/
/*  Device / Browser detection
/*-----------------------------------*/

var check = {

	isIOS: function() {
		if(jQuery('html').hasClass('iphone') || jQuery('html').hasClass('ipad')) {
			return true;
		} else {
			return false;
		}
	},
	isOldIE: function() {
		if(jQuery('html').hasClass('oldie')) {
			return true;
		} else {
			return false;
		}
	},
	isIpad: function() {
		if(jQuery('html').hasClass('ipad')) {
			return true;
		} else {
			return false;
		}
	},
	isIphone: function() {
		if(jQuery('html').hasClass('iphone')) {
			return true;
		} else {
			return false;
		}
	},
	isAndroid: function() {
		var ua = navigator.userAgent.toLowerCase();
		var hasAndroid = ua.indexOf("android") > -1;
		return hasAndroid;
	},
	isTouch: function() {
		return Modernizr.touch;
	}

};



/*-----------------------------------*/
/*  iOS version check
/*-----------------------------------*/

function iOSversion() {
  if (/iP(hone|od|ad)/.test(navigator.platform)) {
    var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
    return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
  }
}

navigator.sayswho = (function(){
    var ua= navigator.userAgent,
    N= navigator.appName, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*([\d\.]+)/i) || [];
    M= M[2]? [M[1], M[2]]:[N, navigator.appVersion, '-?'];
    if(M && (tem= ua.match(/version\/([\.\d]+)/i))!== null) M[2]= tem[1];
    return M.join(' ');
})();



/*-----------------------------------*/
/*  Execute JS based on page class:
/*  see: http://bit.ly/10UzTMI
/*-----------------------------------*/

UTIL = {

  fire : function(func,funcname, args){

    var namespace = site;

    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function'){
      namespace[func][funcname](args);
    }

  },

  loadEvents : function(){

    var bodyId = document.body.id;

    UTIL.fire('common');

    jQuery.each(document.body.className.split(/\s+/),function(i,classnm){
      UTIL.fire(classnm);
      UTIL.fire(classnm,bodyId);
    });

    UTIL.fire('common','finalize');

  }

};

jQuery(document).ready(UTIL.loadEvents);