<?
	/*-----------------------------------*/
	/*  Single blog post
	/*-----------------------------------*/
?>

<?php get_header(); ?>

	<section class="blog">
		<div class="container">

			<?php while ( have_posts() ) : the_post(); ?>
				<article class="clearfix">

					<div class="meta">
						<h1><a href="<? the_permalink() ?>"><? the_title(); ?></a></h1>
						<h2><? the_time('d F Y') ?> <span>in <a href="#"><?php the_category(' & ') ?></a></span></h2>
					</div>

					<div class="post">
					  <?php the_content(); ?>
					</div>

				</article>
			<?php endwhile; ?>

			<? wp_reset_query(); ?>

		</div>
	</section>

<?php get_footer(); ?>