<?php

	/*-----------------------------------*/
	/*  Single Events Template
	/*-----------------------------------*/

?>

<?php get_header(); ?>

<div class="breadcrumbs">
	<div class="container">
		<a href="<?= site_url(); ?>">Home</a> > <a href="<?= site_url(); ?>/whats-on">What's On</a> > <a href="<? the_permalink(); ?>"><? the_title(); ?></a>
	</div>
</div>


<div id="mobile_hero"></div>

<section class="<?= $post->post_name;?>" id="events-single">


	<div class="container">

		<div class="left">


			<?
			/*------------------------------------------------------------------------*/
			/*	Category
			/*------------------------------------------------------------------------*/
			$types = wp_get_post_terms( $post->ID, 'event-types' );
			if(!empty($types)){ ?>
			<h3><?= $types[0]->name; ?></h3>
			<? } else { ?>
			<h3>Event</h3>
			<? } ?>


			<?
			/*------------------------------------------------------------------------*/
			/*	Title
			/*------------------------------------------------------------------------*/
			?>
			<?
			$title = get_the_title();
			$alternate_title = get_field('alternate_title',$post->ID);
			if(!empty($alternate_title)) $title = $alternate_title;
			?>

			<h1><?= $title; ?></h1>



			<div class="meta_info">

				<?
				/*------------------------------------------------------------------------*/
				/*	Venue Info
				/*------------------------------------------------------------------------*/
				?>

				<p class="venue">
					<?
					$venues = get_field('venue');
					if(!empty($venues)){
						foreach( $venues as $post): setup_postdata($post);

				     		the_title();

				     		//build venue data for use in sidebar
				     		$venue['title'] = get_the_title();
				     		$venue['address'] = get_field('address',$post->ID);
				     		$venue['telephone'] = get_field('telephone',$post->ID);
				     		$venue['website'] = get_field('website',$post->ID);
				     		$venue['email'] = get_field('email',$post->ID);
				     		$venue['map_data'] = get_field('map',$post->ID);

				     		break; //for now only catering for 1 venue

				     	endforeach;
				     	wp_reset_postdata();
			     	}
					?>
				</p>

				<?
				/*------------------------------------------------------------------------*/
				/*	Time/Date
				/*------------------------------------------------------------------------*/
				?>

				<p class="timedate">
					<?
					echo get_field('time').', ';

					if(get_field('date_type')=='span'){
						//multiple dates
						//see functions.php
						echo event_date_format( get_field('date_from'), get_field('date_to') );

					} else {
						//single date
						$date_from = DateTime::createFromFormat('Ymd', get_field('date_from'));
						echo $date_from->format('d F Y');
					}
					?>
				</p>


				<?
				/*------------------------------------------------------------------------*/
				/*	Ticket Prices
				/*------------------------------------------------------------------------*/
				?>

				<?php if( get_field('ticket_prices') ): ?>
					<p>Tickets <?php the_field('ticket_prices'); ?></p>
				<?php endif; ?>




			</div>



			<div id="mobile_share"></div>


			<?
			/*------------------------------------------------------------------------*/
			/*	Hero Image
			/*------------------------------------------------------------------------*/
			?>

			<div class="hero_image">
				<?
		        $attachment_id = get_field('hero_image');
		        $hero_img = wp_get_attachment_image_src( $attachment_id, 'large');
		        ?>
		        <img src="<?= $hero_img[0]; ?>" />
			</div>


			<?
			/*------------------------------------------------------------------------*/
			/*	Intro
			/*------------------------------------------------------------------------*/

			$intro_text = get_field('intro_text');
			if(!empty($intro_text)){ ?>
				<div class="intro">
				<? echo $intro_text; ?>
				</div>
			<? } ?>



			<?
			/*------------------------------------------------------------------------*/
			/*	Flexible Content
			/*------------------------------------------------------------------------*/

			$content_var = 'content';
			include(locate_template('_inc/inc.content-loop.php'));
			?>



			<?
			/*------------------------------------------------------------------------*/
			/*	Eventbrite
			/*------------------------------------------------------------------------*/

			$eventbrite = get_field('eventbrite');
			if(!empty($eventbrite)){ ?>
				<div class="eventbrite">
				<h2>Book on Eventbrite</h2>
				<? echo $eventbrite; ?>
				</div>
			<? } ?>



			<?
			/*------------------------------------------------------------------------*/
			/*	File Downloads
			/*------------------------------------------------------------------------*/

			if( have_rows('file_downloads') ): ?>
				<div class="file_downloads">
					<h2>Download related material</h2>
					<ul>
					<?
					while( have_rows('file_downloads') ): the_row();

						$file_id = get_sub_field('file');
						$file_path = wp_get_attachment_url( $file_id );
						$linking_text = get_sub_field('title');
						$description = get_sub_field('description');
						if(empty($linking_text)) $linking_text = 'Download File';

						if(!empty($file_path)){ ?>
							<li class="file">
								<a class="file_icon" href="<?= $file_path; ?>"><img src="<?php bloginfo('template_directory'); ?>/images/download.jpg"/></a>
								<a href="<?= $file_path; ?>" target="_blank"><?= $linking_text; ?></a>
								<?= $description; ?>
							</li>
						<? } ?>

					<?
					endwhile; ?>
					</ul>
				</div>
			<?
			endif;
			?>






	    </div> <!-- end .left -->





	    <div class="right">

			<div id="location" class="sidebar-widget">

				<h2>Location</h2>

			    <?
			    /*------------------------------------------------------------------------*/
				/*	Venue location map
				/*------------------------------------------------------------------------*/

				if(!empty($venue['map_data'])){ ?>
				<div class="location-map">
					<div class="marker" data-lat="<? echo $venue['map_data']['lat']; ?>" data-lng="<? echo $venue['map_data']['lng']; ?>"></div>
				</div>
				<? } ?>


				<?
				/*------------------------------------------------------------------------*/
				/*	Venue Data
				/*------------------------------------------------------------------------*/
				?>

				<div class="venue_data">
					<h3><?= $venue['title']; ?></h3>
					<p><?= $venue['address']; ?></p>
					<? if(!empty($venue['telephone'])){ ?><p>Telephone: <?= $venue['telephone']; ?></p><? } ?>
					<? if(!empty($venue['email'])){ ?><p>Email: <a href="mailto:<?= $venue['email']; ?>"><?= $venue['email']; ?></a></p><? } ?>
					<? if(!empty($venue['website'])){ ?><p>Website: <a href="<?= $venue['website']; ?>"><?= $venue['website']; ?></a></p><? } ?>
				</div>
			</div>


				<?
				/*------------------------------------------------------------------------*/
				/*	Share
				/*------------------------------------------------------------------------*/
				?>

				<div class="share_event">
					<h2>Share this event</h2>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(the_permalink()); ?>"><i class="fa fa-facebook fa-2x"></i></a>
					<a href="https://twitter.com/intent/tweet?url=<?= urlencode(the_permalink()); ?>"><i class="fa fa-twitter fa-2x"></i></a>
					<a href="http://pinterest.com/pin/create/button/?url=<?= urlencode(the_permalink()); ?>'&media=<?= $hero_img[0]; ?>&description=<? the_title(); ?>"><i class="fa fa-pinterest fa-2x"></i></a>
					<a href="http://www.tumblr.com/share?v=3&u=<?= urlencode(the_permalink()); ?>"><i class="fa fa-tumblr fa-2x"></i></a>
				</div>



	    	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
			<script type="text/javascript">
			(function($) {



				/*-----------------------------------*/
				/*  Map Functions
				/*-----------------------------------*/

				function render_map( $el ) {

					// var
					var $markers = $el.find('.marker');

					// vars
					var args = {
						zoom		: 16,
						center		: new google.maps.LatLng(0, 0),
						mapTypeId	: google.maps.MapTypeId.ROADMAP
					};

					// create map
					var map = new google.maps.Map( $el[0], args);

					// add a markers reference
					map.markers = [];

					// add markers
					$markers.each(function(){
						add_marker( $(this), map );
					});

					// center map
					center_map( map );

				}

				function add_marker( $marker, map ) {

					// var
					var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

					// create marker
					var marker = new google.maps.Marker({
						position	: latlng,
						map			: map
					});

					// add to array
					map.markers.push( marker );

					// if marker contains HTML, add it to an infoWindow
					if( $marker.html() )
					{
						// create info window
						var infowindow = new google.maps.InfoWindow({
							content		: $marker.html()
						});

						// show info window when marker is clicked
						google.maps.event.addListener(marker, 'click', function() {

							infowindow.open( map, marker );

						});
					}

				}

				function center_map( map ) {

					var bounds = new google.maps.LatLngBounds();

					// loop through all markers and create bounds
					$.each( map.markers, function( i, marker ){

						var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
						bounds.extend( latlng );
					});

					// only 1 marker?
					if( map.markers.length == 1 ){
						// set center of map
						map.setCenter( bounds.getCenter() );
						map.setZoom( 16 );
					} else {
						// fit to bounds
						map.fitBounds( bounds );
					}

				}




				$(document).ready(function(){

					$('.location-map').each(function(){

						render_map( $(this) );

					});


				});

			})(jQuery);
			</script>

	    </div> <!-- end .right -->

	</div>
	<div class="outer-container">

			<?
			/*------------------------------------------------------------------------*/
			/*	you might also be interested in
			/*------------------------------------------------------------------------*/

			$i = 1;
			$you_might_also_like = get_field('you_might_also_like');
			if(!empty($you_might_also_like)){ ?>

				<div class="also_events">

					<h2>You might also be interested in</h2>

					<?
					foreach( $you_might_also_like as $post): setup_postdata($post);

			     		include(locate_template('_inc/inc.event-item.php'));

			     		$i++;
			     		if($i>3) break;
			     	endforeach;
			     	wp_reset_postdata();
			     	?>
		     	</div>

		    <? } ?>

		</div>


</section>


<?php get_footer(); ?>