<?php
/*------------------------------------------------------------------------*/
/*	Register and load js
/*------------------------------------------------------------------------*/

add_action('wp_enqueue_scripts', function(){

	wp_enqueue_script('jquery');
	wp_enqueue_script('init', get_template_directory_uri() . '/js/min/init.min.js', 'jquery', '', false);
	wp_enqueue_script('plugins', get_template_directory_uri() . '/js/min/plugins.min.js', 'jquery', '', true);
	wp_enqueue_script('scripts', get_template_directory_uri() . '/js/min/scripts.min.js', 'jquery', '', true);

	//ajax
	//wp_localize_script('scripts', 'my_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
});



/*------------------------------------------------------------------------*/
/*  Enqueue css
/*------------------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', function(){
	 wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css?v=1' );
});