<?
/*------------------------------------------------------------------------*/
/*	Custom Post Types
/*------------------------------------------------------------------------*/
function register_projects() {
    register_post_type('projects', array(
        'labels' => array(
	        'menu_position' => 3,
            'name' => 'Projects',
            'singular_name' => 'Project',
            'add_new' => 'Add new project',
            'edit_item' => 'Edit project',
            'new_item' => 'New project',
            'view_item' => 'View project',
            'search_items' => 'Search projects',
            'not_found' => 'No projects found',
            'not_found_in_trash' => 'No project found in Trash'
        ),
        'public' => true,
        'supports' => array(
            'title'
        )
    ));
    register_taxonomy(
    	'disciplines',
	    'projects',
	    array(
	        'label' => __('Disciplines'),
	        'singular_label' => __('Discipline'),
	        'hierarchical' => true,
	        'query_var' => true,
	        'rewrite' => true,
	        'show_in_nav_menus' => true,
	    )
	);

}
add_action('init', 'register_projects');