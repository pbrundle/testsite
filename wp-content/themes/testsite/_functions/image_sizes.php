<?php


/*------------------------------------------------------------------------*/
/*	Register custom image sizes for WP to process (X, Y, Force Crop)
/*------------------------------------------------------------------------*/

if ( function_exists( 'add_image_size' ) ) {

	add_image_size('s', 320, 180, true ); 
	add_image_size('m', 640, 360, true ); 

	
	//add_image_size('service_thumb', 600, 420, true ); 
	//add_image_size('full_width_slider', 1280, 450, true ); 
}
