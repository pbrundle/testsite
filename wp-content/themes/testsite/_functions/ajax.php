<?

/*------------------------------------------------------------------------*/
/*	AJAX SETUP
/*  remember to enqueue ajax in _functions/enqueue.php
/*------------------------------------------------------------------------*/


if ( is_user_logged_in() ){
   $action_prefix = 'wp_ajax_';
} else {
   $action_prefix = 'wp_ajax_nopriv_';
}



/*------------------------------------------------------------------------*/
/*	AJAX functions
/*------------------------------------------------------------------------*/


add_action($action_prefix.'email_submission', function(){


	$data = array();
	parse_str($_POST['vals'], $data);

	//print_r($data);


	//send email 

	$to = 'pete@studiopda.com.au';  // get_field('work_with_us-email_address','options');
	$subject = 'Enquiry from website';
	$headers = array('Content-Type: text/html; charset=UTF-8');

	$message = '<p><strong>Date</strong><br />'.date('Y-m-d H:i:s').'</p>';
	$message .= '<p><strong>IP Address</strong><br />'.getIP().'</p>';
	if(!empty($data['full_name'])) $message .= '<p><strong>Name</strong><br />'.$data['full_name'].'</p>';
	if(!empty($data['email'])) $message .= '<p><strong>Email</strong><br /><a href="mailto:'. $data['email'].'">'. $data['email'].'</a></p>';
	if(!empty($data['telephone'])) $message .= '<p><strong>Telephone</strong><br />'.$data['telephone'].'</p>';


	if(wp_mail( $to, $subject, $message, $headers )){
		$success = 1;
	} else {
		$success = 0;
	}

	echo json_encode( array('success' => $success) );
	exit;

});