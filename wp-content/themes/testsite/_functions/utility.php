<?


/*------------------------------------------------------------------------*/
/*	Event Date Formatting
/*------------------------------------------------------------------------*/

function event_date_format($from,$to){

	$date_from = DateTime::createFromFormat('Ymd',$from);
	$date_to = DateTime::createFromFormat('Ymd', $to);

	//different years
	if($date_from->format('Y') < $date_to->format('Y')){
		return $date_from->format('d M Y') . ' - '.$date_to->format('d M Y');

	//same year, but not same month
	} elseif($date_from->format('Y') == $date_to->format('Y') && $date_from->format('m') != $date_to->format('m')){
		return $date_from->format('d M') . ' - '.$date_to->format('d M') . ' ' .$date_to->format('Y');

	//same year and same month
	} elseif($date_from->format('Y') == $date_to->format('Y') && $date_from->format('m') == $date_to->format('m')){
		return $date_from->format('d') . ' - '.$date_to->format('d') . ' ' .$date_to->format('M Y');
	}

	//otherwise
	return $date_from->format('d M Y') . ' - '.$date_to->format('d M Y');
}




/*------------------------------------------------------------------------*/
/*	Event Date Span Query Addition  (used in www.ccc.dev)
/*	https://wordpress.org/support/topic/order-posts-on-meta_query-with-relation
/*------------------------------------------------------------------------*/

function get_meta_sql_datespans( $pieces ) {

	global $wpdb;

	$query = " AND $wpdb->postmeta.meta_key = 'date_from'
		AND (mt1.meta_key = 'date_from' OR mt1.meta_key = 'date_to')
		AND mt1.meta_value  >= %s";

	$pieces['where'] = $wpdb->prepare( $query, date( 'Ymd' ) );

	return $pieces;
}
	


/*------------------------------------------------------------------------*/
/*	Customize Adjacent Post Link Order (menu_order)
/*  for use with 'simple page ordering' plugin
/*------------------------------------------------------------------------*/


function my_custom_adjacent_post_where($sql) {
	if ( !is_main_query() || !is_singular() )
		return $sql;
	
	$the_post = get_post( get_the_ID() );
	$patterns = array();
	$patterns[] = '/post_date/';
	$patterns[] = '/\'[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\'/';
	$replacements = array();
	$replacements[] = 'menu_order';
	$replacements[] = $the_post->menu_order;
	return preg_replace( $patterns, $replacements, $sql );
}
add_filter( 'get_next_post_where', 'my_custom_adjacent_post_where' );
add_filter( 'get_previous_post_where', 'my_custom_adjacent_post_where' );
 
function my_custom_adjacent_post_sort($sql) {
	if ( !is_main_query() || !is_singular() )
		return $sql;
	
	$pattern = '/post_date/';
	$replacement = 'menu_order';
	return preg_replace( $pattern, $replacement, $sql );
}
add_filter( 'get_next_post_sort', 'my_custom_adjacent_post_sort' );
add_filter( 'get_previous_post_sort', 'my_custom_adjacent_post_sort' );



/*------------------------------------------------------------------------*/
/*	Get Custom Taxonomy Slug (Useful for inline filtering of posts)
/*------------------------------------------------------------------------*/

function custom_taxonomies_slug() {
	global $post, $post_id;
	// get post by post id
	$post = &get_post($post->ID);
	// get post type by post
	$post_type = $post->post_type;
	// get post type taxonomies
	$taxonomies = get_object_taxonomies($post_type);
	foreach ($taxonomies as $taxonomy) {
	// get the terms related to post
	$terms = get_the_terms( $post->ID, $taxonomy );
	if ( !empty( $terms ) ) {
		$out = array();
		foreach ( $terms as $term )
			$out[] = $term->slug;
		$return = join( ' ', $out );
	}
	return $return;
	}
}



/*------------------------------------------------------------------------*/
/*	Get Post ID from Slug
/*------------------------------------------------------------------------*/

function get_post_id_from_slug($slug) {
	$page = get_page_by_path($slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}


/*------------------------------------------------------------------------*/
/*	Get Attachment ID from URL
/*------------------------------------------------------------------------*/

function get_attachment_id_from_url( $attachment_url = '' ) {

	global $wpdb;
	$attachment_id = false;

	// If there is no url, return.
	if ( '' == $attachment_url )
		return;

	// Get the upload directory paths
	$upload_dir_paths = wp_upload_dir();

	// Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
	if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

		// If this is the URL of an auto-generated thumbnail, get the URL of the original image
		$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

		// Remove the upload path base directory from the attachment URL
		$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

		// Finally, run a custom database query to get the attachment ID from the modified attachment URL
		$attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );

	}

	return $attachment_id;
}



/*------------------------------------------------------------------------*/
/*	Character Limiter  (breaks at end of word)
/*------------------------------------------------------------------------*/

function characterLimit($content, $limit=40){

    $content = preg_replace(" (\[.*?\])",'',$content);
    $content = strip_shortcodes($content);
    $content = strip_tags($content);
    $content = substr($content, 0, $limit);
    $content = substr($content, 0, strripos($content, " "));
    $content = trim(preg_replace( '/\s+/', ' ', $content));
    $content = $content.'...';
    return $content;
}
