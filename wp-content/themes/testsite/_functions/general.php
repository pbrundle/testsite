<?php

/*------------------------------------------------------------------------*/
/*	Set Timezone
/*------------------------------------------------------------------------*/
date_default_timezone_set('Australia/Melbourne');



/*------------------------------------------------------------------------*/
/*	Remove superfluous menu items (eg. links, comments)
/*------------------------------------------------------------------------*/

function remove_menu_items() {
  global $menu;
  $restricted = array(__('Links'), __('Comments'), __('Posts'));
  end ($menu);
  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
      unset($menu[key($menu)]);}
    }
  }
add_action('admin_menu', 'remove_menu_items');



/*------------------------------------------------------------------------*/
/*	Hide WP Maintenance Mode From Plugin Update
/*------------------------------------------------------------------------*/

/*	function filter_plugin_updates( $value ) {
    unset( $value->response['wp-maintenance-mode/wp-maintenance-mode.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );
*/




/*------------------------------------------------------------------------*/
/*	Add a custom logo to the login screen
/*------------------------------------------------------------------------*/

function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/images/logo.png) !important; padding-bottom:30px !important; 
        width:62px !important; height:61px !important; margin:0 auto !important; background-size:62px !important; }
        </style>';
}
add_action('login_head', 'my_custom_login_logo');


/*------------------------------------------------------------------------*/
/*	hide WP update notice to all but admin users
/*------------------------------------------------------------------------*/

function hide_update_notice_to_all_but_admin_users()
{
    if (!current_user_can('update_core')) {
        remove_action( 'admin_notices', 'update_nag', 3 );
    }
}
add_action( 'admin_notices', 'hide_update_notice_to_all_but_admin_users', 1 );


/*------------------------------------------------------------------------*/
/*	Add Customised ACF Wysiwyg Toolbar options
/*------------------------------------------------------------------------*/

function my_toolbars( $toolbars ){

	// tinymce toolbar items: http://www.tinymce.com/wiki.php/Controls

	//newdocument bold italic underline strikethrough alignleft aligncenter alignright alignjustify 
	//styleselect formatselect fontselect fontsizeselect cut copy paste bullist numlist outdent indent 
	//blockquote undo redo removeformat subscript superscript 
	//link unlink code fullscreen nonbreaking emoticons table 

	// Add a new toolbar called "Very Simple"
	$toolbars['Very Simple' ] = array();
	$toolbars['Very Simple' ][1] = array('bold', 'italic', 'underline', 'bullist', 'link', 'unlink' ,'code');

	// Edit the "Full" toolbar and remove 'code'
	// (http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key)
	if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false ){
	    unset( $toolbars['Full' ][2][$key] );
	}
	// remove the 'Basic' toolbar completely
	unset( $toolbars['Basic' ] );
	// return $toolbars
	return $toolbars;
}
add_filter('acf/fields/wysiwyg/toolbars','my_toolbars');

