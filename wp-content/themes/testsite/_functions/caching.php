<?
/*------------------------------------------------------------------------*/
/*	Services List Data - cached
/*------------------------------------------------------------------------*/


function services_list_data($cache_active=true){

	global $post;
	$transient = 'services_list_data';
	$cache_expiry = 1440;


	if($cache_expiry > 0){
		$data = get_transient($transient);
	}  else {
		delete_transient($transient);
		$data = false;
	}

	if($data === false || !$cache_active) {


		$args = array(
			'post_type' => 'services',
			'posts_per_page' => -1,
			'orderby'	 => 'menu_order', // using Simple Page Ordering plugin
			'order'	 => 'asc'
		);

		$query = new WP_Query($args);
		$data = array();
		$i = 0;
		while ($query->have_posts()) : $query->the_post(); 

			$data[$i]['id'] = get_the_id();
			$data[$i]['title'] = get_the_title();
			$data[$i]['permalink'] = get_the_permalink();
			$data[$i]['image'] = wp_get_attachment_image_src( get_field('hero_image'), 'service_thumb' );
	
		$i++;
		endwhile;
		wp_reset_query(); 


		// SET CACHE
		if($cache_expiry>0){
			set_transient( $transient, $data, 60 * $cache_expiry );
		} else {
			delete_transient( $transient );
		}
	}

	return $data;
}



/*------------------------------------------------------------------------*/
/*  purge cache on page update
/*------------------------------------------------------------------------*/

function purge_cache_on_page_update( $post_id ) {

	if ( wp_is_post_revision( $post_id ) )
	return;

	$post = get_post($post_id);
	//$slug = $post->post_name;
	//print_r($post);

	if($post->post_type=='services'){
		delete_transient( 'services_list_data' );
	}

}
add_action( 'save_post', 'purge_cache_on_page_update' );