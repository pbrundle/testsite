<?

/*------------------------------------------------------------------------*/
/*	Custom Admin Columns
/*------------------------------------------------------------------------*/


// STAFF

add_filter("manage_edit-staff_columns", function($columns){
	$columns = array(
		'cb' => '<input type="checkbox" />',		
		'title' => 'Title',
		'role' =>	'Role',
		'qualifications' =>	'Qualifications'
					
	);
	return $columns;
});

add_action("manage_staff_posts_custom_column", function($column){

	global $post;

	if($column == 'qualifications'){
		the_field('Qualifications',$post->ID);

	} elseif($column == 'role'){
		$terms = wp_get_object_terms( $post->ID,  'roles' );
		$c = count($terms);
		$i = 0;
		foreach($terms as $term){
			$i++;
			echo $term->name;
			if($i<$c) echo ', ';
		}
	}
});



// NEWSLETTERS

add_filter("manage_edit-newsletters_columns", function($columns){
	
	$columns = array(
		'cb' => '<input type="checkbox" />',		
		'title' => 'Title',
		'date1' =>	'Date'
					
	);
	return $columns;
});

add_action("manage_newsletters_posts_custom_column", function($column){

	global $post;
	if($column == 'date1'){
		$date = get_field('date',$post->ID);
		$date = DateTime::createFromFormat('M Y',$date);
		echo $date->format('F Y');
	}	
});

add_filter( 'manage_edit-newsletters_sortable_columns',  function($columns){

	$columns['date1'] = 'date1';
	return $columns;
});

add_action( 'pre_get_posts', function( $query ) {  

    $orderby = $query->get( 'orderby');  
    if( 'date1' == $orderby ) {  
        $query->set('meta_key','date');  
        $query->set('orderby','meta_value_num'); 
        $query->set('order','desc');  
    }  
});
