<!doctype html>
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html <? language_attributes(); ?> ><!--<![endif]-->

	<head>

		<meta charset="<? bloginfo( 'charset' ); ?>" />

		<title><?php wp_title('&mdash;', true, 'right'); ?><?php bloginfo('name'); ?></title>

		<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script type="text/javascript">
			var templateDir = "<?php bloginfo('template_directory'); ?>";
			var baseURL = "<?php bloginfo('wpurl'); ?>";
		</script>

		<? wp_head(); ?>

	</head>

	<body <? body_class(); ?>>

		<header>
			<div class="container">

				<div id="logo">
					<h1><a href="<?= site_url(); ?>">Base theme</a></h1>
				</div>

				<nav>
					<?php wp_nav_menu(array('menu' => 'main_menu')); ?>
				</nav>

			</div>
		</header>
